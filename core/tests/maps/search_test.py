from django.test import TestCase
from rest_framework.reverse import reverse_lazy

from core import models


class SearchTest(TestCase):
    url = reverse_lazy('search_map')

    def setUp(self):
        username, password = 'tester', '123'

        self.user = models.User.objects.create_user(username, 'tester@ya.ta', password)
        self.user.maps.create(name='new_123')
        self.user.maps.create(name='new_13')
        self.user.maps.create(name='new113')

        self.client.login(username=self.user.username, password=password)

    def test_user_unlogin(self):

        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_success(self):

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(set(response.context['object_list']), set(self.user.maps.all()))
