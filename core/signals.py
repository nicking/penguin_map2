def delete_file(sender, instance, **kwargs):

    if hasattr(instance, 'file'):
        storage, path = instance.file.storage, instance.file.path
        storage.delete(path)

    if hasattr(instance, 'image'):
        storage, path = instance.image.storage, instance.image.path
        storage.delete(path)
