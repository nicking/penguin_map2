from PIL import Image


def lead_to_desired_size(pic_path, from_size, to_size, to_path=None): # from_size=(1024, 768), to_size=(2592, 1944)
    if not to_path:
        to_path = pic_path

    # привести к нужному размеру
    image = Image.open(pic_path)
    width, length = image.size

    if width > length:
        new_length = from_size[1]
        new_width = new_length * (width/length)
        if new_width > to_size[0]:
            new_width = to_size[0]

    else:
        new_width = from_size[0]
        new_length = new_width * (length/width)
        if new_length > to_size[1]:
            new_length = to_size[1]

    new_size = [int(new_width), int(new_length)]
    resize(file_path=pic_path, to_path=to_path, size=new_size)


def resize(file_path, size, to_path=None):
    if not to_path:
        to_path = file_path

    img = Image.open(file_path)

    save_kwargs = {}
    if img.info.get('exif'):
        save_kwargs['exif'] = img.info['exif']

    img = img.resize(size, Image.ANTIALIAS)
    type_file = file_path.split('.').pop().upper()

    if type_file == 'JPG':
        type_file = 'JPEG'

    img.save(to_path, type_file, **save_kwargs)
