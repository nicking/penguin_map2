from django import forms

from core import models


class MapModelForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=models.User.objects.all(), widget=forms.HiddenInput)

    class Meta:
        model = models.Map
        exclude = ('map_size_x', 'map_size_y')
