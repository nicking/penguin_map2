from django.contrib import admin
from core import models


admin.site.register(models.Picture)
admin.site.register(models.Sprite)
admin.site.register(models.User)
