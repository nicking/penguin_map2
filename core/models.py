from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import F
from django.db.models.signals import post_delete
import core.signals


class User(AbstractUser):

    email = models.EmailField(unique=True, blank=False)

    class Meta:
        verbose_name = 'Пользователя'
        verbose_name_plural = 'Пользователи'


class Map(models.Model):
    name = models.CharField(max_length=70, verbose_name='Название')
    text = models.TextField(verbose_name='Описание', blank=True)
    dc = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    map_size_x = models.IntegerField(verbose_name='x', blank=True, null=True)
    map_size_y = models.IntegerField(verbose_name='y', blank=True, null=True)
    # background = models.ImageField(verbose_name='фон', upload_to='map-backgrounds/', blank=True)
    user = models.ForeignKey(User, related_name='maps', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        obj = super(Map, self).save(*args, **kwargs)
        map = Map.objects.get(id=self.id)
        if not map.layers.filter(z_index=1):
            map.layers.create(z_index=1)
        return obj

    class Meta:
        verbose_name = 'Карта'
        verbose_name_plural = 'Карты'
        unique_together = ('user', 'name')


class Layer(models.Model):
    name = models.CharField(max_length=150, blank=True)
    z_index = models.PositiveSmallIntegerField()
    map = models.ForeignKey(Map, related_name='layers')

    def delete(self, **kwargs):
        map = self.map
        z_index = self.z_index
        obj = super().delete(**kwargs)

        map.layers.filter(z_index__gt=z_index).update(z_index=F('z_index') - 1)
        if map.layers.count() < 1:
            map.layers.create(z_index=1)
        return obj

    class Meta:
        verbose_name = 'Слой карты'
        verbose_name_plural = 'Слои карты'
        ordering = ('z_index', )


class Picture(models.Model):
    image = models.ImageField(verbose_name='картинка', upload_to='image-files/')
    user = models.ForeignKey(User, related_name='pictures')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
post_delete.connect(core.signals.delete_file, sender=Picture)


class Sprite(models.Model):
    image = models.ImageField(verbose_name='картинка спрайта', upload_to='sprite-files/')
    count_images = models.IntegerField(default=1, verbose_name='Количество картинок в спрайте')
    height = models.IntegerField(verbose_name='Высота одного спрайта (не нужно указывать)', blank=True,  null=True)
    start_height = models.IntegerField(verbose_name='Высота, с которой начинается спрайт', blank=True, null=True)
    speed = models.IntegerField(help_text='чем меньше - тем выше скорость',
          validators=[MaxValueValidator(5), MinValueValidator(0)], default=2)

    user = models.ForeignKey(User, related_name='sprites')

    class Meta:
        verbose_name = 'Спрайт'
        verbose_name_plural = 'Спрайты'
post_delete.connect(core.signals.delete_file, sender=Sprite)


class LayerObject(models.Model):
    layer = None # need add relation name

    x = models.FloatField(verbose_name='x')
    y = models.FloatField(verbose_name='y')
    width = models.FloatField()
    height = models.FloatField()
    rotation = models.FloatField(default=0, help_text='PI -180 -0- 180; 0 - вверх')

    class Meta:
        abstract = True

    def get_serialize_data(self):
        return {
            'id': self.id,
            'x': self.x,
            'y': self.y,
            'width': self.width,
            'height': self.height,
            'rotation': self.rotation
        }


class LayerPicture(LayerObject, models.Model):

    layer = models.ForeignKey(Layer, related_name='pictures')
    picture = models.ForeignKey(Picture, related_name='layers')

    class Meta:
        verbose_name = 'Тайл карты'
        verbose_name_plural = 'Тайлы карты'

    def get_serialize_data(self):
        serialize_data = super(LayerPicture, self).get_serialize_data()
        serialize_data.update({

            'picture': self.picture.id,
            'src': self.picture.image.url,

            'layer': self.layer.id
        })
        return serialize_data


class LayerSprite(LayerObject, models.Model):

    layer = models.ForeignKey(Layer, related_name='sprites')
    sprite = models.ForeignKey(Sprite, related_name='layers')

    list_points = models.TextField()

    def get_serialize_data(self):
        serialize_data = super(LayerSprite, self).get_serialize_data()
        serialize_data.update({
            'sprite': self.sprite.id,
            'src': self.sprite.image.url,
            'numberOfFrames': self.sprite.count_images,
            'startHeight': self.sprite.start_height,
            'heightSprite': self.sprite.height,
            'speed': self.sprite.speed,

            'layer': self.layer.id,
        })
        return serialize_data