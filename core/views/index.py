from django.views.generic import TemplateView


class Index(TemplateView):

    def get_template_names(self):
        if self.request.user.is_authenticated:
            return 'core/index/authenticated_index.html'
        else:
            return 'core/index/not_authenticated_index.html'

index = Index.as_view()
