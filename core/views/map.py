from django.urls import reverse
from django.views.generic import DetailView, ListView, CreateView
from core import forms
from core import models
from core.tools.views import LoginRequiredMixin


class Search(LoginRequiredMixin, ListView):
    model = models.Map
    template_name = 'core/map/search.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(user=self.request.user)

search = Search.as_view()


class Create(LoginRequiredMixin, CreateView):
    model = models.Map
    template_name = 'core/map/create.html'
    form_class = forms.MapModelForm

    def get_success_url(self):
        return reverse('edit_map', kwargs={'pk': self.object.id})

    def get_initial(self):
        initial = super().get_initial()
        initial['user'] = self.request.user
        return initial

create = Create.as_view()


class Info(LoginRequiredMixin, DetailView):
    model = models.Map
    template_name = 'core/map/info.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(user=self.request.user)

info = Info.as_view()


class Edit(LoginRequiredMixin, DetailView):
    model = models.Map
    template_name = 'core/map/edit.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(user=self.request.user)

    # def get_context_data(self, **kwargs):
    #     c = super().get_context_data(**kwargs)
        # tiles = serializers.serialize('json', self.object.tiles.all().annotate(src=F('image__url')))
        # print(tiles)
        # c['tiles'] = tiles
        # return c

edit = Edit.as_view()


