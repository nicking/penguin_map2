import json
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import View
from core import models
from core.tools.views import LoginRequiredMixin


class Get(View):
    
    def get(self, request):
        map = models.Map.objects.get(id=request.GET['PENGUIN_MAP_ID'])
        layers = [{'z_index': layer.z_index, 'id': layer.id} for layer in map.layers.all()]
        return HttpResponse(json.dumps(layers))

get = Get.as_view()


class Create(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        name = self.request.POST['name']
        map_id = self.request.POST['map_id']
        map = models.Map.objects.get(id=map_id)

        if self.request.user == map.user:
            map.layers.create(name=name, z_index=(map.layers.count()+1))
            return redirect('edit_map', pk=map_id)

create = Create.as_view()


class Edit(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        name = self.request.POST['new_name']
        map_id = self.request.POST['map_id']
        map = models.Map.objects.get(id=map_id)
        layer = models.Layer.objects.get(id=self.request.POST['layer_id'])

        if self.request.user == map.user:
            layer.name = name
            layer.save()
            return redirect('edit_map', pk=map_id)

edit = Edit.as_view()


class Delete(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        id = self.request.POST['id']
        map_id = self.request.POST['map_id']
        map = models.Map.objects.get(id=map_id)
        if self.request.user == map.user:
            models.Layer.objects.get(id=id).delete()
            return redirect('edit_map', pk=map_id)

delete = Delete.as_view()


class LayerTiles(View):

    def get(self, request, **kwargs):
        map = models.Map.objects.get(id=request.GET['PENGUIN_MAP_ID'])
        layer = map.layers.get(z_index=request.GET['layer_z_index'])
        pictures = [pic.get_serialize_data() for pic in layer.pictures.all()]
        return HttpResponse(json.dumps(pictures))

layer_tiles = LayerTiles.as_view()


class LayerSprites(View):

    def get(self, request, **kwargs):
        map = models.Map.objects.get(id=request.GET['PENGUIN_MAP_ID'])
        layer = map.layers.get(z_index=request.GET['layer_z_index'])
        layer_sprites = [lay_sprite.get_serialize_data() for lay_sprite in layer.sprites.all()]
        return HttpResponse(json.dumps(layer_sprites))

layer_sprites = LayerSprites.as_view()
