import json

from django.http import HttpResponse
from django.http import JsonResponse
from django.views import View
from core.tools.views import LoginRequiredMixin

from core import models


class AddObject(LoginRequiredMixin, View):

    def post(self, request, **kwargs):
        user = self.request.user
        map = models.Map.objects.filter(user=user).get(id=request.POST['PENGUIN_MAP_ID'])
        layer = map.layers.get(id=request.POST['layer_id'])
        properties = json.loads(request.POST['objectProperties'])

        if properties['typeObject'] == 'sprite':
            new_object = self.create_sprite(layer, properties)

        elif properties['typeObject'] == 'picture':
            new_object = self.create_picture(layer, properties)

        return JsonResponse({'id': new_object.id})

    def create_picture(self, layer, properties):

        picture = models.Picture.objects.get(id=properties['id'])
        layer_picture = layer.pictures.create(picture=picture,
                                  x=properties['x'], y=properties['y'],
                                  width=properties['width'], height=properties['height']
                                  )
        return layer_picture

    def create_sprite(self, layer, properties):
        sprite = models.Sprite.objects.get(id=properties['id'])
        layer_sprite = layer.sprites.create(sprite=sprite,
                                            x=properties['x'], y=properties['y'],
                                            width=properties['width'], height=properties['height']
                                            )
        return layer_sprite

add_object = AddObject.as_view()


class UpdateObject(LoginRequiredMixin, View):

    def post(self, request, **kwargs):

        properties = json.loads(request.POST['objectProperties'])
        if properties['typeObject'] == 'picture':
            layer_object = models.LayerPicture.objects.get(id=properties['id'])
        elif properties['typeObject'] == 'sprite':
            layer_object = models.LayerSprite.objects.get(id=properties['id'])

        if self.request.user == layer_object.layer.map.user:
            layer_object.x = properties['x']
            layer_object.y = properties['y']
            layer_object.width = properties['width']
            layer_object.height = properties['height']
            layer_object.rotation = properties['rotation']
            layer_object.save()

            return HttpResponse('success')

update_object = UpdateObject.as_view()


class DeleteObject(LoginRequiredMixin, View):
    def post(self, request, **kwargs):
        properties = json.loads(request.POST['objectProperties'])
        if properties['typeObject'] == 'picture':
            picture_id = properties['id']
            layer_picture = models.LayerPicture.objects.get(id=picture_id)
            if self.request.user == layer_picture.layer.map.user:
                layer_picture.delete()
                return HttpResponse('success')

        elif properties['typeObject']:
            sprite_id = properties['id']
            layer_sprite = models.LayerSprite.objects.get(id=sprite_id)
            if self.request.user == layer_sprite.layer.map.user:
                layer_sprite.delete()
                return HttpResponse('success')

delete_object = DeleteObject.as_view()
