from django.conf.urls import url
from core.views import index, map, map_edit, layer


urlpatterns = [
    url(r'^$', index.index, name='index'),
    url(r'^maps/$', map.search, name='search_map'),
    url(r'^maps/create/$', map.create, name='create_map'),
    url(r'^maps/(?P<pk>\w+)/$', map.info, name='info_map'),
    url(r'^maps/(?P<pk>\w+)/edit/$', map.edit, name='edit_map'),

    url(r'^map/add_object/$', map_edit.add_object, name='add_object'),
    url(r'^map/update_object/$', map_edit.update_object, name='update_object'),
    url(r'^map/delete_object/$', map_edit.delete_object, name='delete_object'),

    url(r'^layer/get/$', layer.get, name='layer_get'),
    url(r'^layer/create/$', layer.create, name='layer_create'),
    url(r'^layer/edit/$', layer.edit, name='layer_edit'),
    url(r'^layer/delete/$', layer.delete, name='layer_delete'),

    url(r'^layer/layer_tiles/$', layer.layer_tiles, name='layer_tiles'),
    url(r'^layer/layer_sprites/$', layer.layer_sprites, name='layer_sprites'),
]
