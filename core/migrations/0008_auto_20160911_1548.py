# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-11 15:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20160911_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sprite',
            name='height',
            field=models.IntegerField(null=True, verbose_name='Высота одного спрайта (не нужно указывать)'),
        ),
        migrations.AlterField(
            model_name='sprite',
            name='start_height',
            field=models.IntegerField(null=True, verbose_name='Высота, с которой начинается спрайт'),
        ),
    ]
