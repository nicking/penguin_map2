# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-15 10:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20160911_1548'),
    ]

    operations = [
        migrations.AddField(
            model_name='layersprite',
            name='layer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='sprites', to='core.Layer'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='layersprite',
            name='list_points',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='layersprite',
            name='sprite',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='layer_sprites', to='core.Sprite'),
        ),
        migrations.AlterField(
            model_name='sprite',
            name='height',
            field=models.IntegerField(blank=True, null=True, verbose_name='Высота одного спрайта (не нужно указывать)'),
        ),
        migrations.AlterField(
            model_name='sprite',
            name='start_height',
            field=models.IntegerField(blank=True, null=True, verbose_name='Высота, с которой начинается спрайт'),
        ),
    ]
