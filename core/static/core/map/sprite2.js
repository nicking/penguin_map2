function Sprite2(spriteOption, layer) {

    this.typeObject = 'sprite';
    this.id = spriteOption.id || undefined;

    this.x = spriteOption.x;
    this.y = spriteOption.y;
    this.width = spriteOption.width;
    this.height = spriteOption.height;
    this.rotation = spriteOption.rotation || 0;
    this.drawBuffer = 0; // for increase sprite on canvas

    this.numberOfFrames = spriteOption.numberOfFrames || 1;
    this.startHeight = spriteOption.startHeight;
    this.heightSprite = spriteOption.heightSprite;
    this.widthTotalSprite = undefined; // after load image
    this.speed = spriteOption.speed || 2; // after load image

    this.image = new Image();
    var self = this;
    this.image.onload = function () {
        self.onload();
        layer.sprites.push(self)
    };
    this.image.src = spriteOption.src;
    this.onload(); //  два раза выполнится onload (не знаю как исправить)

    this.pathPoints = spriteOption.pathPoints || [[this.x, this.y], [this.x, this.y]];

    this.frameIndex = 0;
    this.scorer = 0;

    return this
}

Sprite2.prototype.onload = function () {
    if (!this.heightSprite || this.heightSprite === 'None') {
        this.heightSprite = this.image.height
    }
    if (!this.startHeight || this.startHeight === 'None') {
        this.startHeight = 0
    }
    if (!this.widthTotalSprite) {
        this.widthTotalSprite = this.image.width;
    }

    if (!this.width) {
        this.width = this.image.width / this.numberOfFrames;
    }
    if (!this.height) {
        this.height = this.heightSprite;
    }
};

Sprite2.prototype.draw = function (canvas) {
    this.render(canvas);
    this.loop()
};

Sprite2.prototype.loop = function () {
    this.updateScorer();
    this.updateIndexFrame();
};

Sprite2.prototype.updateScorer = function () {
    var scorer = 50;
    if (this.scorer < scorer) {
        this.scorer += 1;
    } else {
        this.scorer = 0;
    }
};

Sprite2.prototype.render = function (canvas) {
    var self = this;
    canvas.context.drawImage(
        self.image,
        //choose rect on pic
        self.widthTotalSprite / self.numberOfFrames * self.frameIndex,
        self.startHeight,
        self.widthTotalSprite / self.numberOfFrames,
        self.heightSprite,
        //choose position on canvas
        self.x - this.drawBuffer,
        self.y - this.drawBuffer,
        self.width + this.drawBuffer,
        self.height + this.drawBuffer
    );
};

Sprite2.prototype.updateIndexFrame = function () {
    var speedSprite = this.speed;
    if (this.scorer % speedSprite === 0) {
        this.frameIndex += 1;

        if (this.frameIndex >= this.numberOfFrames) {
            this.frameIndex = 0;
        }
    }
};

Sprite2.prototype.getProperties = function () {
    return {
        x: this.x,
        y: this.y,
        width: this.width,
        height: this.height,
        rotation: this.rotation,

        numberOfFrames: this.numberOfFrames,
        startHeight: this.startHeight,
        heightSprite: this.heightSprite,
        widthTotalSprite: this.widthTotalSprite,
        speed: this.speed,

        pathPoints: this.pathPoints,
        id: this.id,

        typeObject: this.typeObject
    }
};