function Layer(layerProperties, canvas) {
    this.z_index = layerProperties.z_index;
    this.id = layerProperties.id;
    this.checked = layerProperties.checked || true;

    this.canvas = canvas;

    this.pictures = [];
    this.sprites = [];

    this.getObjects()
}

Layer.prototype.drawObjects = function () {
    this.drawPictures();
    this.drawSprites();
};

Layer.prototype.drawPictures = function () {
    var self = this;
    this.pictures.forEach(function (picture) {
        picture.draw(self.canvas);
    })
};

Layer.prototype.drawSprites = function () {
    var self = this;
    this.sprites.forEach(function (sprite) {
        sprite.draw(self.canvas);
    })
};

Layer.prototype.getObjects = function () {
    var self = this;

    $.ajax({
        // url: self.canvas.PENGUIN_MAP_HOST + '/layer/layer_tiles/',
        url: '/layer/layer_tiles/',
        data: {PENGUIN_MAP_ID: self.canvas.PENGUIN_MAP_ID, layer_z_index: this.z_index}
    }).done(function (response) {
        var pics = JSON.parse(response);
        pics.forEach(function (picData) {
            // self.pictures.push(new Picture(picData))
            new Picture(picData, self)
        })
    });

    $.ajax({
        // url: self.canvas.PENGUIN_MAP_HOST + '/layer/layer_sprites/',
        url: '/layer/layer_sprites/',
        data: {PENGUIN_MAP_ID: self.canvas.PENGUIN_MAP_ID, layer_z_index: this.z_index}
    }).done(function (response) {
        var sprites = JSON.parse(response);
        sprites.forEach(function (spriteData) {
            // self.pictures.push(new Sprite2(spriteData))
            new Sprite2(spriteData, self)
        })
    });
};