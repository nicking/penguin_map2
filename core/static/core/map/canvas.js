function Canvas(target, PENGUIN_MAP_ID) {
    this.target = target;
    this.PENGUIN_MAP_ID = PENGUIN_MAP_ID;

    this.PENGUIN_MAP_HOST = 'http://0.0.0.0:8000';

    this.jqCanvas = $('<canvas>');
    this.target.append(this.jqCanvas);

    this.canvasElement = this.jqCanvas[0];
    this.context = this.canvasElement.getContext("2d");

    this.setUp();

    this.layers = [];
    this.drawLayers = [];

    this.mousedownEvents = [];
    this.mousemoveEvents = [];
    this.mouseupEvents = [];
    this.dblclickEvents = [];

    this.getLayers();

    this.listenCanvasEvents();
    this.drawing(); //
    return this
}

Canvas.prototype.setUp = function () {
    // this.canvas.width(document.body.clientWidth);
    // this.canvas.height(document.body.clientHeight);
    this.jqCanvas.width(this.target.width());
    this.jqCanvas.height(this.target.height());

    this.canvasElement.width = this.target.width();
    this.canvasElement.height = this.target.height();
};

Canvas.prototype.drawing = function () {
    var self = this;
    var DRAWING_SPEED = 60; // then less then cooler - чем меньше, тем круче
    setInterval(function () {
        self.context.clearRect(0, 0, self.jqCanvas.width(), self.jqCanvas.height());

        self.drawLayers.forEach(function (layer) {

            layer.drawObjects()
        });
    }, DRAWING_SPEED); //
};

Canvas.prototype.listenCanvasEvents = function () {
    var self = this;
    // self.canvasElement.addEventListener("mousedown", self.click.bind(self), false);      ###self.click - event func
    self.canvasElement.addEventListener("mousedown", self.getEventsFunc(this.mousedownEvents).bind(self), false);
    self.canvasElement.addEventListener("mousemove", self.getEventsFunc(this.mousemoveEvents).bind(self), false);
    self.canvasElement.addEventListener("mouseup", self.getEventsFunc(this.mouseupEvents).bind(self), false);
    self.canvasElement.addEventListener("dblclick", self.getEventsFunc(this.dblclickEvents).bind(self), false);
};

Canvas.prototype.getEventsFunc = function (listEvents) {
    return function (event) {
        var x = event.offsetX;
        var y = event.offsetY;
        // var x = event.clientX;
        // var y = event.clientY;
        // var x = event.screenX;
        // var y = event.screenY;

        // var coords = relMouseCoords(event);

        var canvas = this;

        // console.log(event);
        listEvents.forEach(function (event) {
            event(x, y, canvas)
        });
    }
};

Canvas.prototype.clearEvents = function () {
    this.mousedownEvents.length = 0;
    this.mouseupEvents.length = 0;
    this.mousemoveEvents.length = 0;
    this.dblclickEvents.length = 0;
};

Canvas.prototype.getLayers = function () {
    var self = this;
    $.ajax({
        // url: self.PENGUIN_MAP_HOST + '/layer/get/',
        url: '/layer/get/',
        async: false,
        data: {PENGUIN_MAP_ID: this.PENGUIN_MAP_ID}
    }).done(function (response) {

        var layers = JSON.parse(response);
        layers.forEach(function (layerData) {
            var layer = new Layer(layerData, self);
            self.layers.push(layer);
            self.drawLayers.push(layer)
        })
    });
};

Canvas.prototype.getLayerByz_index = function (z_index) {
    var chooseLayer = undefined;
    this.layers.forEach(function (layer) {
        if (layer.z_index == z_index) {
            chooseLayer = layer
        }
    });
    if (!chooseLayer) {
        alert('Что то пошло не так! Слоя не существует')
    }
    return chooseLayer
};