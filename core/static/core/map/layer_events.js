function LayerEvents(canvas) {
    this.canvas = canvas;

    this.currentLayer = this.getCheckedLayer();
    console.log(this.currentLayer);
    this.addLayerEvents();
    this.addObjectEvents();
}

LayerEvents.prototype.getCheckedLayer = function () {
    var self = this;
    var checkedInput = $('input[type=radio][name=chooseLayerEdit][checked=checked]');
    var z_index = checkedInput.parent().parent().attr('data-z_index');
    return self.canvas.getLayerByz_index(z_index);
};

LayerEvents.prototype.addLayerEvents = function () {
    var self = this;
    $('input[type=radio][name=chooseLayerEdit]').change(function () {
        var z_index = $(this).parent().parent().attr('data-z_index');
        self.currentLayer = self.canvas.getLayerByz_index(z_index);
    });
    $('input[type=checkbox][class=chooseLayerDraw]').change(function () {
        self.canvas.drawLayers = [];

        $('input[type=checkbox][class=chooseLayerDraw]:checked')
            .each(function (index, element) {
                var z_index = $(element).parent().parent().attr('data-z_index');
                self.canvas.drawLayers.push(self.canvas.getLayerByz_index(z_index))
            });

        // self.currentLayer = self.canvas.getLayerByz_index(z_index);
    })
};

LayerEvents.prototype.addObjectEvents = function () {
    var self = this;
    $('#clearEvents').click(function () {
        alert('clear');
        self.canvas.clearEvents()
    });
    $('#addBuffer').click(function () {
        alert('addBuffer');
        self.canvas.clearEvents();
        self.canvas.mousemoveEvents.push(self.objectAddDrawBuffer())
    });
    $('#AddObject').click(function () {
        alert('AddPicture');
        self.canvas.clearEvents();
        self.canvas.mousedownEvents.push(self.objectAdd(
            $('.UserPictures'), $('.UserSprites')
        ));
        canvas.mousemoveEvents.push(self.objectAddDrawBuffer())
    });
    $('#EditSizeObject').click(function () {
        alert('EditSizePicture');
        self.canvas.clearEvents();
        self.canvas.mousemoveEvents.push(self.objectAddDrawBuffer());

        var editSizeObjects = self.editSizeObject();
        self.canvas.mousedownEvents.push(editSizeObjects.objectsFindEvent);
        self.canvas.mousemoveEvents.push(editSizeObjects.objectsEditSizeEvent);
        self.canvas.mouseupEvents.push(editSizeObjects.objectsStopEvent);
    });
    $('#DragObject').click(function () {
        alert('DragObject');
        self.canvas.clearEvents();
        self.canvas.mousemoveEvents.push(self.objectAddDrawBuffer());

        var dragObjectEvents = self.moveObject();
        self.canvas.mousedownEvents.push(dragObjectEvents.objectsFindEvent);
        self.canvas.mousemoveEvents.push(dragObjectEvents.objectsMoveEvent);
        self.canvas.mouseupEvents.push(dragObjectEvents.objectsStopMoveEvent);
    });
    $('#rotationPicture').click(function () {
        alert('rotationPicture');
        self.canvas.clearEvents();
        self.canvas.mousemoveEvents.push(self.objectAddDrawBuffer());

        var dragPictureEvents = self.editRotationPicture();
        self.canvas.mousedownEvents.push(dragPictureEvents.pictureFindEvent);
        self.canvas.mousemoveEvents.push(dragPictureEvents.pictureEditRotationEvent);
        self.canvas.mouseupEvents.push(dragPictureEvents.pictureStopEvent);
    });
    $('#deleteObject').click(function () {
        self.canvas.clearEvents();
        self.canvas.mousemoveEvents.push(self.objectAddDrawBuffer());

        self.canvas.mousedownEvents.push(self.deleteObject())
    });
};

LayerEvents.prototype.objectAdd = function (targetPhotos, targetSprites) {
    //canvas.clickEvents.push(LayerEvents.pictureAddEvent($('.responsive_img img')))
    var photoSrc, photoID;
    var spriteSrc, spriteID, numberOfFrames, heightSprite, startHeight, speed;
    var self = this;

    targetPhotos.click(function () {
        photoSrc = $(this).attr('src');
        photoID = $(this).attr('id').split('_').pop();

        spriteSrc = undefined
    });
    targetSprites.click(function () {
        var targetSprite = $(this);
        spriteSrc = targetSprite.attr('src');
        spriteID = targetSprite.attr('id').split('_').pop();
        numberOfFrames = targetSprite.attr('data-numberOfFrames');
        heightSprite = targetSprite.attr('data-height');
        startHeight = targetSprite.attr('data-startHeight');
        speed = targetSprite.attr('data-spedd');

        photoSrc = undefined
    });

    function createPicture(x, y) {
        var pictureOptions = {x: x, y: y, src: photoSrc, id: photoID};
        return new Picture(pictureOptions, self.currentLayer);
        // self.currentLayer.pictures.push(picture);
    }

    function createSpite(x, y, canvas) {
        var spriteOptions = {
            x: x, y: y, src: spriteSrc, id: spriteID,
            numberOfFrames: numberOfFrames, speed: speed,
            heightSprite: heightSprite, startHeight: startHeight
        };

        return new Sprite2(spriteOptions, self.currentLayer);
        // self.currentLayer.sprites.push(sprite);
        // return sprite;
    }

    return function (x, y, canvas) {
        if (!photoSrc && !spriteSrc) {
            return
        }
        var object;
        if (photoSrc) {
            object = createPicture(x, y, canvas)
        } else if (spriteSrc) {
            object = createSpite(x, y, canvas)
        }

        $.ajax({
            method: "POST",
            url: '/map/add_object/',
            data: {
                PENGUIN_MAP_ID: self.canvas.PENGUIN_MAP_ID,
                layer_id: self.currentLayer.id,
                objectProperties: JSON.stringify(object.getProperties()),
                csrfmiddlewaretoken: $.cookie('csrftoken')
            }
        }).done(function (response) {
            object.id = response.id
        });
    }
};

LayerEvents.prototype.deleteObject = function () {
    var self = this;
    return function (x, y, canvas) {
        function requestDeleteObject(object, deleteFromList) {
            $.ajax({
                method: "POST",
                url: '/map/delete_object/',
                data: {
                    objectProperties: JSON.stringify(object.getProperties()),
                    csrfmiddlewaretoken: $.cookie('csrftoken')
                }
            }).done(function () {
                    var index = deleteFromList.indexOf(object);
                    deleteFromList.splice(index, 1);
                }
            );
        }

        var selectedPictures = self.getSelectedPictures(x, y);
        selectedPictures.forEach(function (picture) {
            requestDeleteObject(picture, self.currentLayer.pictures)
        });
        var selectedSprites = self.getSelectedSprites(x, y);
        selectedSprites.forEach(function (sprite) {
            requestDeleteObject(sprite, self.currentLayer.sprites)
        })
    }
};

LayerEvents.prototype.moveObject = function () {
    var dragPictures = [];
    var dragSprites = [];

    var xMouseFrom = undefined, yMouseFrom = undefined;
    var self = this;

    var objectsFindEvent = function (x, y, canvas) {
        dragPictures = self.getSelectedPictures(x, y);
        dragSprites = self.getSelectedSprites(x, y);
        xMouseFrom = x;
        yMouseFrom = y;
    };

    var objectsMoveEvent = function (xMouseTo, yMouseTo, canvas) {

        if (xMouseFrom === undefined || yMouseFrom === undefined) {
        } else {
            var dX = xMouseTo - xMouseFrom;
            var dY = yMouseTo - yMouseFrom;
            dragPictures.forEach(function (picture) {
                picture.x += dX;
                picture.y += dY;
            });
            dragSprites.forEach(function (sprite) {
                sprite.x += dX;
                sprite.y += dY;
            });
            xMouseFrom = xMouseTo;
            yMouseFrom = yMouseTo;
        }
    };

    var sendUpdateObject = function (object) {
        $.ajax({
            method: "POST",
            url: '/map/update_object/',
            data: {
                objectProperties: JSON.stringify(object.getProperties()),
                csrfmiddlewaretoken: $.cookie('csrftoken')
            }
        }).done(function (response) {
                console.log(response)
            }
        );
    };
    var objectsStopMoveEvent = function (x, y, canvas) {

        if (xMouseFrom && yMouseFrom) {
            dragPictures.forEach(function (picture) {
                sendUpdateObject(picture)
            });
            dragSprites.forEach(function (sprite) {
                sendUpdateObject(sprite)
            });
        }

        xMouseFrom = undefined;
        yMouseFrom = undefined;
        dragPictures = [];
    };
    return {
        objectsFindEvent: objectsFindEvent,
        objectsMoveEvent: objectsMoveEvent,
        objectsStopMoveEvent: objectsStopMoveEvent
    }
};

LayerEvents.prototype.editSizeObject = function () {
    var editPictures = [];
    var editSprites = [];

    var xMouseFrom = undefined, yMouseFrom = undefined;
    var self = this;

    var objectsFindEvent = function (x, y, canvas) {
        editPictures = self.getSelectedPictures(x, y);
        editSprites = self.getSelectedSprites(x, y);
        xMouseFrom = x;
        yMouseFrom = y;
    };
    var objectsEditSizeEvent = function (xMouseTo, yMouseTo, canvas) {

        if (xMouseFrom === undefined || yMouseFrom === undefined) {
        } else {
            var dX = xMouseTo - xMouseFrom;
            var dY = yMouseTo - yMouseFrom;
            editPictures.forEach(function (picture) {
                picture.width += dX;
                picture.height += dY;
            });
            editSprites.forEach(function (picture) {
                picture.width += dX;
                picture.height += dY;
            });
            xMouseFrom = xMouseTo;
            yMouseFrom = yMouseTo;
        }
    };

    var sendUpdateObject = function (object) {
        $.ajax({
            method: "POST",
            url: '/map/update_object/',
            data: {
                objectProperties: JSON.stringify(object.getProperties()),
                csrfmiddlewaretoken: $.cookie('csrftoken')
            }
        }).done(function (response) {
                console.log(response)
            }
        );
    };
    var objectsStopEvent = function (x, y, canvas) {
        editPictures.forEach(function (picture) {
            sendUpdateObject(picture)
        });
        editSprites.forEach(function (sprite) {
            sendUpdateObject(sprite)
        });

        xMouseFrom = undefined;
        yMouseFrom = undefined;
        editPictures = [];
    };
    return {
        objectsFindEvent: objectsFindEvent,
        objectsEditSizeEvent: objectsEditSizeEvent,
        objectsStopEvent: objectsStopEvent
    }
};

LayerEvents.prototype.editRotationPicture = function () {
    // ДОДЕЛАТЬ НОРМАЛЬНО ЧТОБ
    var rotationPictures = [];

    var xMouseFrom = undefined,
        yMouseFrom = undefined;

    var self = this;

    var pictureFindEvent = function (x, y, canvas) {
        rotationPictures = self.getSelectedPictures(x, y);
        xMouseFrom = x;
        yMouseFrom = y;
    };
    var pictureEditRotationEvent = function (xMouseTo, yMouseTo, canvas) {

        if (xMouseFrom === undefined || yMouseFrom === undefined) {
        } else {
            var dX = xMouseTo - xMouseFrom;
            var dY = yMouseTo - xMouseFrom;

            // canvas.context.rect(xMouseFrom,xMouseFrom,20,20);
            canvas.context.fillRect(xMouseFrom, xMouseFrom, 20, 20);

            var degree = Math.atan(dY / dX) * 180 / Math.PI;
            // console.log(degree);
            rotationPictures.forEach(function (picture) {
                picture.rotation = degree;
            });
        }
    };
    var pictureStopEvent = function (x, y, canvas) {
        console.log('pictureStopEvent');

        rotationPictures.forEach(function (picture) {
            var pictureProperties = picture.getProperties();
            $.ajax({
                method: "POST",
                url: '/map/update_object/',
                data: {
                    objectProperties: JSON.stringify(pictureProperties),
                    // pictureProperties: JSON.stringify(pictureProperties),
                    csrfmiddlewaretoken: $.cookie('csrftoken')
                }
            }).done(function (response) {
                    console.log(response)
                }
            );
        });

        xMouseFrom = undefined;
        yMouseFrom = undefined;
        rotationPictures = [];
    };
    return {
        pictureFindEvent: pictureFindEvent,
        pictureEditRotationEvent: pictureEditRotationEvent,
        pictureStopEvent: pictureStopEvent
    }
};

LayerEvents.prototype.objectAddDrawBuffer = function () {
    var selectedOldPictures = [];
    var selectedOldSprites = [];

    var self = this;
    return function (x, y, canvas) {
        var currentSelectedPictures = self.getSelectedPictures(x, y);
        currentSelectedPictures.forEach(function (pic) {
            pic.drawBuffer = 5;
        });
        var currentSelectedSprites = self.getSelectedSprites(x, y);
        currentSelectedSprites.forEach(function (sprite) {
            sprite.drawBuffer = 5;
        });

        selectedOldPictures.forEach(function (oldPic) {
            if (currentSelectedPictures.indexOf(oldPic) < 0) {
                oldPic.drawBuffer = 0
            }
        });
        selectedOldSprites.forEach(function (oldSprite) {
            if (currentSelectedSprites.indexOf(oldSprite) < 0) {
                oldSprite.drawBuffer = 0
            }
        });
        selectedOldPictures = currentSelectedPictures;
        selectedOldSprites = currentSelectedSprites;
    };
};

LayerEvents.prototype.getSelectedPictures = function (x, y) {
    var selectedPictures = [];
    this.currentLayer.pictures.forEach(function (picture) {
        if ((x <= picture.x + picture.width) && (x >= picture.x) &&
            (y <= picture.y + picture.height) && (y >= picture.y)
        ) {
            selectedPictures.push(picture)
        }
    });
    return selectedPictures
};
LayerEvents.prototype.getSelectedSprites = function (x, y, canvas) {
    var selectedSprites = [];
    this.currentLayer.sprites.forEach(function (sprite) {
        if ((x <= sprite.x + sprite.width) && (x >= sprite.x) &&
            (y <= sprite.y + sprite.height) && (y >= sprite.y)
        ) {
            selectedSprites.push(sprite)
        }
    });
    return selectedSprites
};