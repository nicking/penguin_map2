function Picture(pic_options, layer) {
    this.typeObject = 'picture';
    this.src = pic_options.src;
    this.id = pic_options.id;

    this.x = pic_options.x;
    this.y = pic_options.y;

    this.drawBuffer = 0; // for increase pic on canvas

    this.width = pic_options.width;
    this.height = pic_options.height;
    this.rotation = pic_options.rotation; // degree;  -180 0 180//  0 - up 90 - right


    this.image = new Image();
    var self = this;
    this.image.onload = function () {
        self.onload();
        layer.pictures.push(self)
    };
    this.image.src = pic_options.src;
    this.onload();
}

Picture.prototype.onload = function () {
    if (!this.width) {
        this.width = this.image.width;
        console.log(this.image.width)
    }
    if (!this.height) {
        this.height = this.image.height
    }
    if (!this.rotation) {
        this.rotation = 0
    }
};

Picture.prototype.draw = function (canvas) {
    if (this.rotation !== 0) {
        canvas.context.save();
        canvas.context.rotate((Math.PI / 180) * this.rotation);
    }
    canvas.context.drawImage(this.image,
        0, 0, this.image.width, this.image.height,
        this.x - this.drawBuffer, this.y - this.drawBuffer,
        this.width + this.drawBuffer, this.height + this.drawBuffer);

    if (this.rotation !== 0) {
        canvas.context.restore()
    }
};

Picture.prototype.getProperties = function () {
    return {
        src: this.src,
        id: this.id,
        x: this.x,
        y: this.y,
        width: this.width,
        height: this.height,
        rotation: this.rotation,

        typeObject: this.typeObject
    }
};

